import os
import sys
import traceback

from PyQt5.QtCore import QObject, QRunnable, QThreadPool, pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import (QApplication, QFileDialog, QGridLayout,
                             QHBoxLayout, QLabel, QLineEdit, QPlainTextEdit,
                             QPushButton, QSpinBox, QVBoxLayout, QWidget)

import main
import threading


class WorkerSignals(QObject):
    """
    Communication between Worker and App
    """
    finished = pyqtSignal()
    error = pyqtSignal(tuple)
    prnt = pyqtSignal(tuple)


class Worker(QRunnable):
    def __init__(self, parent, ev):
        super(Worker, self).__init__()
        self.signals = WorkerSignals()
        self.parent = parent
        self.ev = ev

    @pyqtSlot()
    def run(self):
        try:
            d = main.Downloader(self.signals.prnt, self.ev)
            d.run(username=self.parent.editArtistName.text(),
                  output_path=self.parent.editOutputFolder.text() +
                  self.parent.separator,
                  separator=self.parent.separator,
                  searchterm=self.parent.editFilter.text(),
                  max_download=self.parent.spinboxMaxDownload.value())
        except Exception:
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, traceback.format_exc()))

        self.signals.finished.emit()


class App(QWidget):
    def __init__(self):
        super().__init__()
        self.title = 'Deviantart Bulk Download Tool'
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480
        # self.setStyleSheet(
        #    "background-color: rgb(50, 50, 50); color: rgb(200, 200, 200);")
        self.threadpool = QThreadPool()
        self.initUI()

    def initUI(self):
        self.p = False
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        layout = QVBoxLayout()
        layoutRequired = QGridLayout()
        layoutOptional = QGridLayout()

        self.separator = "/"
        if os.name == "nt":
            self.separator = "\\"

        self.editArtistName = QLineEdit()

        row = 0
        layoutRequired.addWidget(QLabel("Artist Name:"), row, 0)
        layoutRequired.addWidget(self.editArtistName, row, 1)

        self.editOutputFolder = QLineEdit()
        btnOutputFolder = QPushButton("Choose Folder")
        btnOutputFolder.clicked.connect(self.getfiles)

        row += 1
        layoutRequired.addWidget(QLabel("Output Folder:"), row, 0)
        layoutRequired.addWidget(self.editOutputFolder, row, 1)
        layoutRequired.addWidget(btnOutputFolder, row, 2)

        self.editFilter = QLineEdit()

        row = 0
        layoutOptional.addWidget(QLabel("Filter:"), row, 0)
        layoutOptional.addWidget(self.editFilter, row, 1)

        self.spinboxMaxDownload = QSpinBox()

        row += 1
        layoutOptional.addWidget(QLabel("Max Files to download:"), row, 0)
        layoutOptional.addWidget(self.spinboxMaxDownload, row, 1)

        layoutBtns = QHBoxLayout()

        self.outputBox = QPlainTextEdit(self)
        self.lblDownloaded = QLabel("Downloaded: 0")
        self.downloaded = []

        self.lblSkipped = QLabel("Skipped: 0")
        self.skipped = []

        self.btnStart = QPushButton("Start")
        self.btnStart.clicked.connect(self.start)

        self.btnCancel = QPushButton("Abort")
        self.btnCancel.clicked.connect(self.cancel)
        self.btnCancel.setEnabled(False)

        layoutBtns.addWidget(self.btnCancel)
        layoutBtns.addWidget(self.btnStart)

        layout.addWidget(QLabel("Required"))
        layout.addLayout(layoutRequired)
        layout.addWidget(QLabel("Optional"))
        layout.addLayout(layoutOptional)
        layout.addLayout(layoutBtns)
        layout.addWidget(self.lblDownloaded)
        layout.addWidget(self.lblSkipped)
        layout.addWidget(self.outputBox)

        self.stop_event = threading.Event()

        self.setLayout(layout)

        self.show()

    def getfiles(self):
        folder_name = QFileDialog.getExistingDirectory(self)
        if folder_name:
            self.editOutputFolder.setText(folder_name)

    def start(self):
        # in case it's been set before
        self.stop_event.clear()

        self.skipped.clear()
        self.downloaded.clear()

        worker = Worker(self, self.stop_event)
        worker.signals.prnt.connect(self.process_print)
        worker.signals.finished.connect(self.process_done)
        self.threadpool.start(worker)
        self.btnCancel.setEnabled(True)
        self.btnStart.setEnabled(False)

    def process_print(self, s):
        type_ = s[0]
        msg = s[1]

        if type_ == 0:
            self.outputBox.insertPlainText(msg + "\n")
        elif type_ == 1:
            # error message
            pass
        elif type_ == 2:
            # set number of fetched deviants
            self.downloaded.append(msg)
            self.lblDownloaded.setText("Downloaded: " +
                                       str(len(self.downloaded)))
            self.outputBox.insertPlainText("Dowloaded: " + str(msg) + "\n")
        elif type_ == 3 or type_ == 4:  # Deviants that didn't pass through the filter or were already downloaded
            self.skipped.append(msg)
            self.lblSkipped.setText("Skipped: " + str(len(self.skipped)))

    def process_done(self):
        self.btnCancel.setEnabled(False)
        self.btnCancel.setText("Cancel")
        self.btnStart.setEnabled(True)
        self.outputBox.insertPlainText("Done.\n")

    def cancel(self):
        self.btnCancel.setText("Exiting...")
        self.outputBox.insertPlainText("STOP EVENT SET.\n")
        self.stop_event.set()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = App()
sys.exit(app.exec_())
