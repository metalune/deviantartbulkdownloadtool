# DeviantartBulkDownloadTool

## Current state
The GUI is currently rather unstable/untested if you encounter problems with
e.g. aborting a download, please just quit the entire application. I am aware of the problems,
I just didn't have the peace of mind to fix them yet. Thanks

downloaded correctly

## Goals
- [x] GUI (Graphical User Interface)
- [ ] Improve GUI (Graphical User Interface)
- [ ] Add Text Download support
- [ ] Add Video Download support
- [ ] Allow for more complicated download filters, e.g. regex support

## How to run

- Install `python3`
- Install `python3-pip`
- Clone this repository, by either downloading it from the website or typing
  `git clone https://gitlab.com/MoonPadUSer/dbdt.git` in a terminal
- Run `pip3 install -r requirements.txt`
- Run `python3 gui.py`

#### Windows

##### Installing `python` and `pip`
1. Open Powershell from the Start Menu
2. type `python` (and press enter), there should be a Windows Store window opening up, you can install python3 directly from there (by clicking the big fat blue install button)
3. copy this into the powershell `curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py` and press enter, it'll take a bit to download the file
4. type `python get-pip.py` and press enter, it'll take a little bit
5. you can close the powershell now

##### Downloading and running DBDT
1. [Click Here](https://gitlab.com/MoonPadUSer/dbdt/-/archive/master/dbdt-master.zip), it should download the `.zip` file automatically
2. unzip the file
3. go into the directory and double-click `install_deps.bat`, wait until it's done
4. to run the program, double-click `run.bat`

## How to setup

As of now, you need to [register an application](https://gitlab.com/MoonPadUSer/dbdt/-/wikis/How-to-create-a-DeviantArt-Application) on your deviantart account.
The program needs a `client_id` and `client_secret`, you can either put these in
a `config.txt` manually (first `client_id`, then a space and then your `client_secret`), but preferably you'd just run the `main.py` file and
enter the values in the simple setup process.

If you have any questions or issues feel free to write an issue

## Compatibility
In theory, it should work on all Operating Systems, but currently it's only tested
on Windows and Linux

## For potential contributors
I want to follow unix philosophy and all this program will ever do is
download content from deviantart.
