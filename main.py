import shutil
from os import path as os_path

import requests

import deviantart_direct as deviantart

class bcolors:
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    ENDC = '\033[m'


class PrintSignalWrapper:
    def emit(self, msg):
        print(msg[len(msg) - 1])  # print the last element


class ThreadingEventWrapper:
    def is_set(self):
        return False


def download_file(output_path,
                  path,
                  file_name,
                  print_signal=PrintSignalWrapper()):
    # *apparently* you can't have a "|" in a filename,
    # thus replace it with the next best thing (an uppercase i)
    file_name = file_name.replace("|", "I")

    file_path = output_path + file_name + ".jpg"

    if os_path.exists(file_path):
        return False

    print_signal.emit((0, str(bcolors.GREEN + "Downloading " + bcolors.ENDC + file_name)))

    resp = requests.get(path, stream=True)
    local_file = open(file_path, 'wb')
    resp.raw.decode_content = True
    shutil.copyfileobj(resp.raw, local_file)
    del resp
    return True


class Downloader:
    def __init__(self,
                 print_signal=PrintSignalWrapper(),
                 stop_event=ThreadingEventWrapper()):
        """
        signal will be set by GUI to support multithreading
        """
        self.print_signal = print_signal
        self.stop_event = stop_event
        self.loadConfig()
        self.da = deviantart.Api(self.client_id, self.client_secret)

    def run(self,
            username,
            output_path='',
            separator='/',
            searchterm='',
            max_download=-1):
        self.loadConfig()
        self.da = deviantart.Api(self.client_id, self.client_secret)
        self.print_signal.emit((0, "Logged in, starting..."))

        deviations = []
        offset = 0
        has_more = True

        done = False
        while has_more and not done and not self.stop_event.is_set():
            try:
                fetched_deviations = self.da.get_gallery_all(username=username,
                                                             offset=offset)
                new_devs = fetched_deviations['results']
                filtered_devs = []

                for dev in new_devs:
                    if self.stop_event.is_set():
                        return

                    should_download = False

                    if len(searchterm) > 0:
                        if searchterm.lower() == searchterm:
                            # the searchterm is entirely lowercase
                            if searchterm in dev.title.lower():
                                should_download = True
                        else:
                            if searchterm in dev.title:
                                should_download = True
                    else:
                        should_download = True

                    if max_download > 0:
                        if len(filtered_devs) + len(
                                deviations) >= max_download:
                            done = True
                            break

                    if should_download:
                        ret = False
                        try:
                            title = dev.title + " [" + str(dev.deviationid) + "](" + str(dev.published_time) + ")"
                            if dev.is_downloadable:
                                link = self.da.download_deviation(dev)["src"]
                                ret = download_file(output_path, link,
                                                    title)
                            else:
                                link = dev.preview["src"]
                                ret = download_file(output_path, link,
                                                    title)
                        except Exception as e:
                            self.print_signal.emit(
                                (1, "An error has occured downloading\n" +
                                 dev.title + ": \n" + str(e)))
                            continue
                        else:
                            if ret:
                                filtered_devs.append(dev)
                                self.print_signal.emit((2, dev.title))
                            else:
                                self.print_signal.emit((4, dev.title))
                    else:
                        self.print_signal.emit((3, dev.title))
                deviations += filtered_devs

                self.print_signal.emit((-1, "Fetched " + str(len(deviations))))

                offset = fetched_deviations['next_offset']
                has_more = fetched_deviations['has_more']
            except deviantart.api.DeviantartError as e:
                if self.print_signal is not None:
                    self.print_signal.emit((1, str(e)))
                else:
                    print(e)
                if "401" not in str(e):
                    has_more = False

    def loadConfig(self):
        # Load configuration
        with open("config.txt", "r") as file:
            self.client_id = file.readline().strip("\n")
            self.client_secret = file.readline().strip("\n")

        if not self.client_id or not self.client_secret:
            print(
                "Damn it. No configuration file found, please enter configuration data below."
            )
            self.client_id = input("Enter Client ID: ")
            self.client_secret = input("Enter Client Secret: ")

            with open("config.txt", "w") as file:
                file.writeline(self.client_id)
                file.writeline(self.client_secret)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Deviantart Gallery Downloader")
    parser.add_argument("-a",
                        type=str,
                        default='',
                        help="Deviant Artist Username",
                        required=True)
    parser.add_argument("-o",
                        type=str,
                        default='',
                        help='Output Folder Location',
                        required=True)
    parser.add_argument("-s",
                        type=str,
                        default='/',
                        help='File Separator of choice',
                        required=False)
    parser.add_argument(
        "-f",
        type=str,
        default='',
        help=
        'Download Filter, only files thtat match the filter, will be downlaoded',
        required=False)
    parser.add_argument("-m",
                        type=int,
                        default=-1,
                        help="Max Amount of Files to be downloaded",
                        required=False)
    params = vars(parser.parse_args())

    d = Downloader()
    d.run(username=params["a"],
          output_path=params["o"],
          separator=params["s"],
          searchterm=params["f"],
          max_download=params["m"])
